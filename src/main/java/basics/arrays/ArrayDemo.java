package basics.arrays;

import java.util.*;

public class ArrayDemo {

	private static void differentDeclarations() {

		@SuppressWarnings("unused")
		int[] a;
		a = new int[10];

		@SuppressWarnings("unused")
		int b[] = { 1, 20, 3, 5, 6, 7 };

		// Overwriting values on b.
		b = new int[] { 20, 40, 50, 60 };
	}

	private static void arrayIdentity() {
		int[] a = { 23, 45, 66, 78 };
		int[] b = a;

		/*
		 * changing element[1] of a array so that element[1] of c is also
		 * changed as they point to the same array
		 */
		a[1] = 119;

		System.out.println("Values of Array a (original) : " + Arrays.toString(a));
		System.out.println("Values of Array b (alias) :" + Arrays.toString(b));
	}

	private static void actualArrayCopying() {

		int[] numbers = { 3, 4, 6, 7, 8 };

		// Copying array numbers into copiedNumbers using Arrays.copyOf()
		int[] copiedNumbers = Arrays.copyOf(numbers, numbers.length);

		System.out.println("Array copiedNumbers after copying from numbers" + Arrays.toString(copiedNumbers));

		// Increasing the size of Array
		copiedNumbers = Arrays.copyOf(copiedNumbers, 2 * copiedNumbers.length);

		System.out.println("After increasing the length of Array copiedNumbers: " + Arrays.toString(copiedNumbers));
	}

	public static void main(String args[]) {
		differentDeclarations();
		arrayIdentity();
		actualArrayCopying();

		int[] myArray = new int[10];
		initializeWithIndex(myArray);
		System.out.println("After initialization, you get " + Arrays.toString(myArray));
	}

	private static void initializeWithIndex(int[] a) {
		for (int i = 0; i < a.length; i++) {
			a[i] = i;
		}
	}
}
